(* Lexer definition.*)

(* These sections are taken from Real World OCaml. *)

{
	open Lexing
	open Parser

	let next_line lexbuf =
		let pos = lexbuf.lex_curr_p in
		lexbuf.lex_curr_p <-
		{
			pos with pos_bol = lexbuf.lex_curr_pos;
			pos_lnum = pos.pos_lnum + 1
		}
}

let white = [' ' '\t']+
let newline = '\r' | '\n' | "\r\n"

rule tokenize = parse
	| white		{ tokenize lexbuf }
	| newline 	{ next_line lexbuf; tokenize lexbuf }
	| "Ook."	{ STATEMENT }
	| "Ook?"	{ QUESTION }
	| "Ook!"	{ EXCLAMATION }
	| eof		{ EOF }

