(* OokCaml language implementation. *)
(* This file contains the actual implementation; menhir provides the parser. *)

(* Todo: make this better. *)
let filename = Sys.argv.(1) ;;

(* Define the size we should create the table to be. *)
let memory_size = 100 ;;

(* Helper function; gets current value of cell. If not set, returns 0 and sets it. *)
let get_curr_cell memory ptr =
	try
		Hashtbl.find memory ptr
	with
		| Not_found -> let _ = Hashtbl.add memory ptr 0 in 0
	;;

(* Implement bf/ook jump. *)
let rec jump_to_matching commands revcmds count =

	(* Stopping condition: when count = 0. *)
	let rec check_if_done_matching commands revcmds count ook =
		if (count == 0) then
			(commands, (ook :: revcmds))
		else
			jump_to_matching commands (ook :: revcmds) count
	in

	(* Match commands "forward" (sometimes backwards). *)
	match commands with
		| [] -> ([], [])
		| ook :: commands -> match ook with
			| `Jump_zero ->
				let count = count + 1 in
				check_if_done_matching commands revcmds count ook
				
			| `Jump_nonzero ->
				let count = count - 1 in
				check_if_done_matching commands revcmds count ook
		
			| ook ->
				jump_to_matching commands (ook :: revcmds) count
;;

(* Main interpreter function. *)
let rec interpret commands memory ptr revcmds =
	match commands with
		| [] -> 0
		| ook :: commands -> match ook with
			| `Ptr_inc ->
				interpret commands memory (ptr + 1) (ook :: revcmds)

			| `Ptr_dec ->
				interpret commands memory (ptr - 1) (ook :: revcmds)

			| `Inc ->
				let curr = get_curr_cell memory ptr in
				let _ = Hashtbl.replace memory ptr (curr + 1) in
				interpret commands memory ptr (ook :: revcmds)

			| `Dec ->
				let curr = get_curr_cell memory ptr in
				let _ = Hashtbl.replace memory ptr (curr - 1) in
				interpret commands memory ptr (ook :: revcmds)

			| `Putc ->
				let c = Char.chr (get_curr_cell memory ptr) in
				let _ = Printf.printf "%c" c in
				interpret commands memory ptr (ook :: revcmds)

			| `Getc ->
				let c = Char.code (input_char stdin) in
				let _ = Hashtbl.replace memory ptr c in
				interpret commands memory ptr (ook :: revcmds)

			| `Jump_zero ->
				if (get_curr_cell memory ptr == 0) then
					let new_cmds, new_revcmds = jump_to_matching commands (ook :: revcmds) 1 in
					interpret new_cmds memory ptr new_revcmds
				else
					interpret commands memory ptr (ook :: revcmds)

			| `Jump_nonzero ->
				if (get_curr_cell memory ptr != 0) then
					let new_revcmds, new_cmds = jump_to_matching revcmds (ook :: commands) (-1) in
					(* I'm not entirely sure about this logic, there might be a bug. *)
					let new_revcmds = (List.hd new_cmds) :: new_revcmds in
					let new_cmds = List.tl new_cmds in
					interpret new_cmds memory ptr new_revcmds
				else
					interpret commands memory ptr (ook :: revcmds)

			| ookcmd ->
				interpret commands memory ptr (ook :: revcmds)
;;

let main () =

	(* Do tokenization and parsing. This is really easy! *)
	let input = open_in filename in
	let buffer = Lexing.from_channel input in
	let commands = Parser.program Lexer.tokenize buffer in
	
	(* One day we'll write a compiler too, but... this is just a POC
	   for the Verilog parser. *)
	   
	(* Set up interpreter state. Use hash table so negative ptrs work. *)
	let memory = Hashtbl.create memory_size in
	let ptr = 0 in 
	
	(* Run interpreter. *)
	interpret commands memory ptr []
;;

let _ = main ()
