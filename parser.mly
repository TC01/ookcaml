%token STATEMENT
%token QUESTION
%token EXCLAMATION
%token EOF

%start <Ookcmd.ookcmd list> program

%%

program:
	| s = statement EOF { [s] }
	| s = statement m = program { s :: m}

statement:
	| STATEMENT QUESTION		{ `Ptr_inc }
	| QUESTION STATEMENT		{ `Ptr_dec }
	| STATEMENT STATEMENT		{ `Inc }
	| EXCLAMATION EXCLAMATION	{ `Dec }
	| STATEMENT EXCLAMATION		{ `Getc }
	| EXCLAMATION STATEMENT		{ `Putc }
	| QUESTION EXCLAMATION		{ `Jump_nonzero }
	| EXCLAMATION QUESTION      { `Jump_zero }
