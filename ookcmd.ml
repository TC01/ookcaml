(* The commands in question. *)
type ookcmd = [
	| `Ptr_inc
	| `Ptr_dec
	| `Inc
	| `Dec
	| `Getc
	| `Putc
	| `Jump_zero
	| `Jump_nonzero
]
