# OokCaml

This is an [OCaml](https://ocaml.org/) [Menhir](http://gallium.inria.fr/~fpottier/menhir/)
tokenizer, parser, and interpreter for the esoteric programming language [Ook!](https://esolangs.org/wiki/Ook!)
Ook! is just a version of the more well-known esolang [Brainfuck](https://en.wikipedia.org/wiki/Brainfuck);
the commands are named differently but the syntax is identical.

This was written primarily as an exercise in using Menhir and OCaml to parse a
real programming language, with the aid of the Real World OCaml chapter on parsing.
The existing examples shipped with Menhir tend to be for evaluating arithmetic
expressions (e.g. like a basic calculator); this is a "slightly more involved"
example.

A collection of Ook! example programs are included. At the moment there's just
one, really: hello.ook, which prints ("Hello world!").

Future work may involve actually writing a compiler.

## Usage

### Requirements

OokCaml requires OCaml (tested on 4.02.3, any newer version should suffice)
and the "menhir" module. On at least Fedora, this can all be installed from
the distribution's repositories like so:

```
dnf install ocaml ocaml-menhir
```

On other platforms, one can use [opam](http://opam.ocaml.org/) to install Menhir
(and also set up OCaml):

```
opam install menhir
```

### Compiling

OokCaml can be built using ```ocamlbuild``` as follows:

```
ocamlbuild -use-menhir ook.native
```

This will produce a binary called ```ook.native```.

### Running

Simply pass the Ook! source code file as an argument on the command line. A better
command line parser is something that might happen someday. For example, to run
hello.ook:

```
./ook.native examples/hello.ook
```

Any non-whitespace, non-Ook syntax characters will result in a (currently unhelpful)
error message.

# Credits

* Ben Rosser <rosser.bjr@gmail.com>

This program is distributed under the MIT license; please see LICENSE
for the full text.
